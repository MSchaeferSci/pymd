pyMD
====

This project is an educational implementation of a Lennard-Jones fluid simulation.
The NVE as well as the NVT (Langevin, Andersen) ensembles are available.


Requirements
------------

* Python 3.6 or later
* NumPy
* Numba
* Matplotlib


Installation
------------

A setup.py will follow eventually.
Currently it is necessary to git clone the folder.


Example
-------

.. code-block:: python
    import pyMD.main as md
    import pyMD.integrators as mdint

    dt = 0.005
    rho = 0.3
    N = 108
    Neq = 5
    Nt = 3000
    T0 = 3
    output_interval = 20
    drag = 1

    md.simulate(N, rho, T0, dt, Neq, Nt,
                mdint.verletLangevin,
                output_interval,
                drag)


I will try to keep the code modular as it expands, such that it serves as a library from which a user can stitch together a simulation suited for their needs.
Throughout the code reduced units are used (see Frenkel and Smit *Understanding Molecular Simulation*), so a temperature of 1 is approximately equal to 120 K.
The argon atoms are initialized on a simple cubic lattice.
The spacing is determined by the density, rho.
The only interaction is the 12-6 Lennard-Jones potential that is evaluated using the basic O(N^2) all atom algorithm.
The equations of motion are integrated either by the Euler method (NVE) or the velocity verlet method (NVE, Langevin, Arendsen).
Initially, the velocities are rescaled to speed up equilibration Neq-times.


Results
-------

All data was obtained using the parameters of the example, changing only the integrator.

Energy conservation NVE:

.. figure:: img/NVE_energy_conservation.png

    NVE energy conservation



Maxwell-Boltzmann speed distribution from the Langevin dynamics using a draag coefficient of 1.

.. figure:: img/langevin_maxwell_boltzmann.png

    NVT speed distribution

