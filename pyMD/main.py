import pyMD.initialize as pyi
import pyMD.run as pr
import pyMD.mdio as pio

def simulate(N, rho, T0, dt, Neq, Nt, integrator,outputinter=10, gamma=0):
    print("initializing system")
    r0, L = pyi.init_sc(N,rho)
    v0 = pyi.init_vel(N, T0)

    print("equilibrating")
    r, v = pr.equilibrate(r0,v0,dt,T0,L,Neq)

    print("simulating")
    pr.run(r,v,L,dt,Nt,T0,integrator,outputinter,gamma)

    return 0