import numpy as np
import numpy.linalg as npl

def init_sc(N, rho):
    L = (N/rho)**(1/3)

    ni = np.ceil(N**(1/3))
    dx = L / ni

    rd = np.zeros(3)
    r0 = np.zeros((N,3))

    for ii in range(N):
        r0[ii] = rd
        rd[0] += dx
        if rd[0] >= L:
            rd[1] += dx
            rd[0] = 0
        if rd[1] >= L:
            rd[2] += dx
            rd[1] = 0
        if rd[2] >= L:
            raise "Too few lattice points"
    return r0, L


def init_vel(N, T0):
    # set velocities
    v = np.random.randn(N,3)

    # shift velocieties such that total momentum is 0
    vcom = np.sum(v, axis=0) / N
    v -= vcom

    # scale velocities
    v2tot = np.sum(np.linalg.norm(v, axis=1)**2)/ N
    sf = np.sqrt(3 * T0 / v2tot)
    v *= sf
    return v