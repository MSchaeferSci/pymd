import numpy as np
import numpy.linalg as npl

import pyMD.forces as pf


def eulerNVE(r,v,F,dt,L,T,gamma):

    r += v*dt + (dt**2/2)*F
    r = r%L
    v += dt*F
    F, U = pf.U_and_F(r, L)

    return r,v,F,U


def verletNVE(r,v,F,dt,L,T,gamma):

    r += dt* (v + (dt / 2) * F)
    r = r%L
    v += (dt/2) * F
    F,U = pf.U_and_F(r,L)
    v += (dt/2) * F

    return r,v,F,U


def verletLangevin(r,v,F,dt,L,T,gamma):
    N = len(r)
    dW = np.random.randn(N,3) * np.sqrt(dt)
    eps = np.sqrt(2.0 * T * gamma)


    r += dt* (v + (dt / 2) * F)
    r = r%L
    v = v * (1 - gamma * dt/2) + (dt/2) * F
    v += eps * dW / 2
    F,U = pf.U_and_F(r,L)
    v = v * (1 - gamma * dt/2) + (dt/2) * F
    v += eps * dW / 2

    # r += 0.5 * dt * v
    # r = r%L
    # F,U = pf.U_and_F(r,L)
    # v = v * ( 1.0 - gamma * dt) + F * dt + dW * np.sqrt(2.0 * 3.0 * gamma)
    # r += 0.5 * v * dt
    # r = r%L

    return r,v,F,U


def verletAndersen(r,v,F,dt,L,T,nu):

    r += dt* (v + (dt / 2) * F)
    r = r%L
    v += (dt/2) * F
    F,U = pf.U_and_F(r,L)
    v += (dt/2) * F

    N = len(r)
    sigma = np.sqrt(3.0)
    rand = np.random.rand(N)

    for i in range(N):
        if rand[i] < nu * dt:
            v[i] = np.random.normal(loc=0.0,scale=sigma,size=3)

    return r,v,F,U
