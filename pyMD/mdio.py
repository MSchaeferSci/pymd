
class Result:
    def __init__(self, coord_file, result_file,L,measurement):
        self.coord_file  = coord_file
        self.result_file = result_file
        coords = open(self.coord_file, "w")
        coords.close()

        with open(self.result_file, "w") as results:
            results.write(f"iter, ekin, epot, etot, T, vtot\n")
        
        self.L = L

    def update(self,it,r,v,measurement):
        N = len(r)
        ekin = measurement["ekin"]
        epot = measurement["epot"]
        etot = measurement["etot"]
        T = measurement["T"]
        vtot = measurement["vtot"]

        with open(self.coord_file, "a") as coords:
            coords.write(f"{N}\nLattice=\"{self.L} {0.0} {0.0} {0.0} {self.L} {0.0} {0.0} {0.0} {self.L}\"\n")
            for ii in range(N):
                coords.write(f"Ar {r[ii,0]} {r[ii,1]} {r[ii,2]} {v[ii,0]} {v[ii,1]} {v[ii,2]}\n")
            
        with open(self.result_file, "a") as results:
            results.write(f"{it:5}, {ekin:8.5f}, {epot:8.5f}, {etot:8.5f}, {T:8.5f}, {vtot:8.5f}\n")

