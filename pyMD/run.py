import numpy as np
import numpy.linalg as npl
import matplotlib.pyplot as plt

import pyMD.forces as pf
import pyMD.integrators as mdint
import pyMD.mdio as mdio

def equilibrate(r,v,dt,T0,L,Neq):
    N = len(r)

    F, U = pf.U_and_F(r, L)
    for i in range(Neq):
        for j in range(30):
            r,v,F,U = mdint.verletNVE(r,v,F,dt,L,0,0)

        v2tot = sum(np.linalg.norm(v, axis=1)**2)
        lambd = np.sqrt((N+1)*3*T0/v2tot)
        print('lambda = ', lambd)
        v *= lambd
    return r,v


def measure(r,v,F,U):
    N = len(r)
    v2tot = np.sum(npl.norm(v, axis=1)**2)

    vtot = npl.norm(np.sum(v, axis=0))
    epot = U / N
    ekin = 1/2 * v2tot / N
    etot = ekin + epot
    Temp = 1/(3 * N) * v2tot

    measurement = {"epot": epot,
                   "ekin": ekin,
                   "etot": etot,
                   "T"   : Temp,
                   "vtot": vtot}
    return measurement


def mw(v,KT):
        p = (1/(2 * np.pi * KT))**(3/2)
        p *= 4.0 *np.pi * v**2 * np.exp(-v**2 / (2 * KT))
        return p


def run(r,v,L,dt,Nt,T,integrator,outp_inter,gamma):
    
    F, U = pf.U_and_F(r, L)
    measurement = measure(r,v,F,U)
    result = mdio.Result("coords.xyz", "results.csv",L,measurement)

    vv = np.zeros((Nt,len(r)))

    for it in range(Nt):

        r,v,F,U = integrator(r,v,F,dt,L,T,gamma)

        vv[it] = npl.norm(v,axis=1)

        if it % outp_inter == 0:
            measurement = measure(r,v,F,U)
            result.update(it,r,v,measurement)


    s = vv.flatten()
    plt.hist(s, 150, density=True, label="Simulation")

    vw = np.linspace(0,7.5,num=200)
    plt.plot(vw, mw(vw,3.0), label="Theory")
    plt.xlabel("$v$")
    plt.ylabel("$P(v)$")
    plt.show()

    return 0

