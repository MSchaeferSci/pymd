import numpy as np
import numpy.linalg as npl
from numba import jit

@jit(nopython=True)
def U_and_F(r,L):
    N = len(r)
    rc = 2.5
    utail = 8.0/3.0 * 0.3 * ( 1.0/3.0 * rc**(-9) - rc**(-3) )
    ushift = 4 * (rc**(-12) - rc**(-6))

    force = np.zeros((N,3))
    U = 0

    rij = np.zeros(3)
    for i in range(N-1):
        for j in range(i+1,N):
            rij = (r[i] - r[j] + L/2) % L - L/2
            
            d = npl.norm(rij)

            if d < rc:
                Fij = 24.0 * ( 2 * d**(-14) - d**(-8) ) * rij
                force[i] += Fij
                force[j] -= Fij
                U += ( 4 * (d**(-12) - d**(-6)) ) - ushift
    U += N * utail
    return force, U