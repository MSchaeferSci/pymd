import numpy as np
import numpy.linalg as npl
import matplotlib.pyplot as plt
import time

import pyMD.main as md
import pyMD.integrators as mdint

# np.random.seed(1)

start = time.time()

dt = 0.005
rho = 0.3
N = 108
Neq = 5
Nt = 3000
T0 = 3

md.simulate(N, rho, T0, dt, Neq, Nt, mdint.verletLangevin,50,1)

end = time.time()
print("time ", end - start)

dat = np.loadtxt("results.csv",delimiter=",",skiprows=1).T
iter, ekin, epot, etot, T, vtot = dat

print(np.mean(T))

fig = plt.figure()

ax1 = fig.add_subplot(121)
ax2 = fig.add_subplot(122)

ax1.set_ylim([200,420])

ax1.set_xlabel("Iterations")
ax2.set_xlabel("Iterations")

ax1.set_ylabel("T / K")
ax2.set_ylabel("E")

ax1.plot(iter, T*120)
ax2.plot(iter, etot, label="E tot.")
ax2.plot(iter, ekin, label="E kin.")
ax2.plot(iter, epot, label="E pot.")
ax2.legend()
fig.tight_layout()

plt.show()